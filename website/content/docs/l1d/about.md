---
title: "About L1d"
description: "About L1d"
lead: ""
date: 2020-10-06T08:48:57+00:00
lastmod: 2020-10-06T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "l1d"
weight: 10
toc: true
---

{{< figure src="/images/L1d_tube_analysis_plot_m10p0_plot_600dpi_xshift.png" width="100%" alt="space-time diagram for X2 expansion tube"
  caption="<em>Space-time diagram of wave processes in the X2 expansion tube for a Mach 10, high total-pressure operating condition. Simulation and figure preparation by David Gildfind, 2012.</em>" >}}

## What does L1d do?

`l1d` is our specialist simulation code for quasi-one-dimensional gas dynamics.
It is usually applied to the simulation of entire shock-tube or expansion-tube facilities
and has a role to play in the design of new experimental machines.

