---
title: "Development team and contributors"
description: ""
lead: ""
date: 2021-06-30
lastmod: 2021-06-30
draft: false
images: []
menu: 
  docs:
    parent: "introduction"
weight: 40
toc: false
---

## Development Team

Meet the principal developers of the simulation codes.

<div class="row">
   <div class="col-sm-4">
   <img src="/images/rjg-headshot.jpeg" style="width:100%">
   </div>
   <div class="col-sm-12">
   <b>Rowan Gollan</b> is a Senior Lecturer in the School of Mechanical & Mining Engineering
   at The University of Queensland. He is also a member of UQ's Centre for Hypersonics.
   Rowan was awarded a BE (Mechanical & Space) and PhD in Aerospace Engineering from
   The University of Queensland. His PhD thesis was on the modelling of high-temperature
   effects in hypersonic flows. He worked as a Research Scientist at the NASA Langley
   Research Center from 2008-2011 in the Hypersonic Air-Breathing Propulsion Branch.
   Since 2012, he has held various positions at the University of Queensland:
   postdoctoral fellow, ARC DECRA Fellow, Lecturer, and now, Senior Lecturer.
   
   
   Rowan's main research area is computational hypersonics. Specific interests include:
   optimized aerodynamic design; modelling of high-temperature gas effects;
   study of laminar-to-turbulent transition in high-speed flows; 
   development of novel methods for compressible flow simulation; and
   development of compressible flow algorithms on novel architectures.
   
   
   Rowan's involvement with the GDTk code development began in 2001 with an
   undergraduate thesis on subsonic boundary conditions for the 2-D compressible
   flow solver, mb_cns. He now contributes to most aspects of the code base with
   a particular focus on the high-temperature/multi-temperature gas models,
   the Newton-Krylov accelerator, and the adjoint solver.
   </div>
</div>

<br>

<div class="row">
   <div class="col-sm-12">
   <b>Kyle Damm</b> is a Postdoctoral Research Fellow in the School of Mechanical & Mining Engineering at The University of Queensland,
   and a member of UQ's Centre for Hypersonics.
   Kyle was awarded a BE (Mechanical & Aerospace) and Ph.D. in Aerospace Engineering from The University of Queensland.
   His Ph.D. thesis was on adjoint-based optimization in hypersonic flows.
   During his Ph.D., Kyle enjoyed holding visiting research fellow positions in the Aerodynamic Design and Simulation Laboratory
   at Seoul National University and the Hypersonics laboratory at the Korean Advanced Institute of Science and Technology.

   Kyle's main research interests are in the development of novel algorithms for: (1) the simulation of compressible and reacting flows;
   and (2) aerodynamic design optimization.
   He also has interests in automatic differentiation of scientific codes;
   applications of machine learning to computational fluid dynamics;
   and high-performance parallel computing on CPU and GPU architectures.

   Kyle’s involvement with the GDTk code development began in 2014 with an undergraduate thesis
   on the application of GPUs to accelerate the calculation of reacting flow simulations for the compressible flow solver, Eilmer3.
   He now develops and maintains the unstructured code base with a particular focus on the steady-state accelerators (e.g. Newton-Krylov),
   and the adjoint solver.
   </div>
   <div class="col-sm-4">
   <img src="/images/kad-photo.jpeg" style="width:100%">
   </div>
</div>

<br>

<div class="row">
   <div class="col-sm-4">
   <img src="/images/nng-pfp-small.jpg" style="width:100%">
   </div>
   
   <div class="col-sm-12">
   <b>Nick Gibbons</b> an open-source programmer and numerical simulations expert currently employed
   as a postdoctoral research fellow at the University of Queensland.
   He received a Bachelor of Mechanical and Aerospace Engineering in 2014,
   and was awarded a Doctor of Philosophy in 2019 for his work on high speed combustion.
   His research interests include numerical simulation of compressible turbulence,
   high-temperature effects in re-entry flows, and the fluid dynamics of electrically charged plasmas.
   Since 2019, he has worked on GDTk's turbulence modelling, chemical equilibrium calculator,
   and energy mode relaxation machinery, so if you experience any problems with these parts of the code you should probably send him an email.
   You can follow him on Twitter (<a href="https://twitter.com/DrNickNGibbons">@DrNickNGibbons</a>)
   for posts about fluid mechanics, programming, and retweets of trendy web comics.
   </div>
</div>
   
<br>

<div class="row">

   <div class="col-sm-12">
   <b>Peter Jacobs</b> is an Honorary Reader in the School of Mechanical and Mining Engineering
   at the University of Queensland.
   He graduated with a Bachelor Degree in Mechanical Engineering in 1982 and a PhD in 1987.
   He then worked as a research fellow in hypersonics at UQ, then,
   in 1990, 1991 at the Institute for Computer Applications in Science and Engineering, NASA Langley.
   It was there that he learned about methods for simulating compressible gas flows and
   started to write a flow simulation code that would eventually become the Eilmer code.  

   He has been a member of staff at UQ since 1992.
   Over the past thirty years, the main engineering themes in his research
   have been the development of numerical models for the simulation of compressible and reacting flows,
   with special emphasis on hypersonic flight and validation through ground testing facilities.
   In that time he has been a member of the Hypersonics Research Group and the Energy Group within the School of Mechanical and Mining Engineering. 

   He has had the pleasure of working for extended periods with aerospace groups in the USA, the UK, France, Germany, and Japan.
   International exchanges as a member of academic staff have included, Lulea Technical University, Sweden, Tohoku University in Japan,
   and as a Humboldt Fellow at the German Aerospace Research Center in Goettingen, Germany.
   In recent years, he has spent time at the Osney Thermofluids Laboratory at Oxford University
   and the EM2C research group within CNRS, at CentraleSupelec in Paris.
   </div>
   
   <div class="col-sm-4">
   <img src="/images/peterj-in-kelvin-grove-2016-downsampled.jpg" style="width:100%">
   </div>

</div>

<br>

## Contributors

The toolkit has benefited from many contributors over its 30 years of activity.
Some of the main contributors of time and resources to the project are:
+ Ingo Jahn,
+ Chris James,
+ Vince Wheatley,
+ Anand Veeraragavan, and
+ Fabian Zander.

The list below are individuals who have contributed in various ways
such as beta testing, documentation writing, code development, and
bug reporting/feedback (and the patience to work with us on bug fixes).

Ghassan Al'Doori,
Steven Apirana,
Nikhil Banerji,
Justin Beri,
Peter Blyton,
Viv Bone,
Jamie Border,
Arianna Bosco,
Djamel Boutamine,
Laurie Brown,
James Burgess,
David Buttsworth,
Eric WK Chang,
Sam Chiu,
Chris Craddock,
Brian Cook,
Tim Cullen,
Damian Curran,
Jason Czapla,
Andrew Dann,
Andrew Denman,
Zac Denman,
Luke Doherty,
Elise Fahy,
Antonia Flocco,
Delphine Francois,
James Fuata,
David Gildfind,
Richard Gooze&eacute;,
Sangdi Gu,
Birte Haker,
Stefan Hess,
Jonathan Ho,
Jimmy-John Hoste,
Carolyn Jacobs,
Juanita Jacobs,
Chris James,
Ian Johnston,
Ojas Joshi,
Xin Kang,
Rory Kelly,
Rainer Kirchhartz,
Sam Lamboo,
Will Landsberg,
Alexis Lefevre,
Steven Lewis,
Yu (Daisy) Liu,
Kieran Mackle,
Michael Macrossan,
Pierre Mariotto,
Tom Marty,
Matt McGilvray,
Sarah Mecklem,
David Mee,
Carlos de Miranda-Ventura,
Christine Mittler,
Luke Montgomery,
Richard G. Morgan,
Heather Muir,
Jan-Pieter Nap,
Brendan O'Flaherty,
Reece Otto,
Andrew Pastrello,
Oliver Paxton,
Paul Petrie-Repar,
Jorge Sancho Ponce,
Daniel Potter,
Jason (Kan) Qin,
Deepak Ramanath,
Andrew Rowlands,
Jacob Sandral,
Michael Scott,
Umar Sheikh,
Daniel Smith,
Tamara Sopek,
Sam Stennett,
Ben Stewart,
Oliver Street,
Joseph Tang,
Katsu Tanimizu,
Matthew Thompson,
Augustin Tib&egrave;re-Inglesse,
Pierpaolo Toniato,
Matt Trudgian,
Paul van der Laan,
Tjarke van Jindelt,
Jaidev Vesudevan,
Alex Ward,
Han Wei,
Mike Wendt,
Brad Wheatley,
Ryan Whitside,
Lachlan Whyborn,
Adriaan Window,
Hannes Wojciak,
Mengmeng Zhao


