---
title: "Theses"
description: "Theses"
lead: ""
date: 2021-05-26
lastmod: 2021-05-26
draft: false
images: []
menu:
  docs:
    parent: "pub-n-pres"
weight: 30
toc: false
---

The following graduate theses are from members associated with the gdtk group
over the years. The following documents are available in PDF.

1. [Kyle Damm PhD Thesis, 2020](/pdfs/kyle-damm-phd-thesis-feb-2020.pdf) *Adjoint-based aerodynamic design optimisation in hypersonic flow*

1. [Jens Kunze PhD Thesis, 2020](/pdfs/jens-kunze-phd-thesis-feb-2020.pdf) *Design of a 3D shape transitioning nozzle and experimental thrust measurements of an airframe integrated scramjet*

1. [Pierpaolo Toniato PhD Thesis, 2019](/pdfs/pierpaolo-toniato-phd-thesis-2019.pdf) *Free-jet testing of a Mach 12 scramjet in an expansion tube*

1. [Kan (Jason) Qin PhD Thesis, 2017](/pdfs/kan-qin-phd-thesis-oct-2017.pdf) *Development and application of multiphysics simulation tools for foil thrust bearings operating with carbon dioxide*

1. [Elise Fahy PhD Thesis, 2016](/pdfs/elise-fahy-phd-thesis-dec-2016.pdf) *Superorbital Re-entry Shock Layers: Flight and Laboratory Comparisons*

1. [David Gildfind PhD Thesis, 2012](/pdfs/david-gildfind-phd-thesis-oct-2012.pdf) *Development of High Total Pressure Scramjet Flow Conditions using the X2 Expansion Tube*

1. [Wilson Chan PhD Thesis, 2012](/pdfs/wilson-chan-phd-thesis-aug-2012.pdf) *Effects of flow non-uniformities on the drag reduction by boundary layer combustion*

1. [Brendan O'Flaherty PhD Thesis, 2012](/pdfs/brendan-oflaherty-phd-thesis-june-2012.pdf) *Reducing the Global Warming Potential of Coal Mine Ventilation Air by Combustion in a Free-Piston Engine*

1. [Carolyn Jacobs PhD Thesis, 2011](/pdfs/carolyn-jacobs-phd-finalthesis-UQversion-aug-2011.pdf) *Radiation in low density hypervelocity flows*

1. [Dan Potter PhD Thesis, 2011](/pdfs/dan-potter-phd-thesis-may-2011.pdf) *Modeling of radiating shock layers for atmospheric entry at Earth and Mars*

1. [Rowan Gollan PhD Thesis, 2009](/pdfs/rowan-gollan-PhD-thesis-feb-2009.pdf) *The Computational Modelling of High-Temperature Gas Effects with Application to Hypersonic Flows*

1. [Matthew McGilvray PhD Thesis, 2008](/pdfs/matt-mcgilvray-PhD-thesis-sep-2008.pdf) *Scramjet testing at high enthalpies in expansion tube facilities*

1. [Joseph Tang PhD Thesis, 2008](/pdfs/joseph-tang-PhD-thesis-jun-2008.pdf) *Development of a Parallel Adaptive Cartesian Cell Code to Simulate Blast in Complex Geometries*

1. [Matthew Smith PhD Thesis, 2008](/pdfs/matthew-smith-PhD-thesis-june-2008.pdf) *The True Direction Equilibrium Flux Method and its Application*

1. [Adrian Window MPhil Thesis, 2008](/pdfs/adriaan-window-MPhil-thesis-oct-2008.pdf) *Simulation of Separating Flows in the X2 Expansion Tube Over Bluff Aerocapture Vehicles*

1. [Dwishen Ramanah MPhil Thesis, 2007](/pdfs/dwishen-ramanah-mphil_thesis-jan-2007.pdf) *Background Oriented Schlieren Technique for Flow Visualisation in Shock Tunnels*

1. [Andrew Denman PhD Thesis, 2007](/pdfs/andrew-denman-PhD-thesis-jan-2007.pdf) *Large-Eddy Simulation of Compressible Turbulent Boundary Layers with Heat Addition*

1. [Michael Scott PhD Thesis, 2006](/pdfs/michael-scott-phd-thesis-june-2006.pdf) *Development and Modelling of Expansion Tubes*

1. [Michael Elford MSc Thesis, 2005](/pdfs/michael-elford-masters-thesis-sep-2005.pdf) *Validation of a CFD Solver for Hypersonic Flows*

1. [Charles Lilley PhD Thesis, 2005](/pdfs/charles-lilley-phd-thesis-jun-2005.pdf) *A Macroscopic Chemistry Method for the Direct Simulation of Nonequilibrium Gas Flows*

1. [Ben Stewart PhD Thesis, 2004](/pdfs/ben-stewart-phd-thesis-oct-2004.pdf) *Predicted Scramjet Testing Capabilities of the Proposed RHYFL-X Expansion Tube*

1. [Richard Goozee PhD Thesis, 2003](/pdfs/richard-goozee-phd-thesis-apr-2003.pdf) *Simulation of a Complete Shock Tunnel using Parallel Computer Codes*

1. [Kevin Austin PhD Thesis, 2002](/pdfs/kevin-austin-phd-thesis-june-2002.pdf) *Evolutionary Design of Robust Flight Control for a Hypersonic Aircraft*

1. [Vince Wheatley MSc Thesis, 2001](/pdfs/vince-wheatley-masters-thesis-aug-2001.pdf) *Modelling Low-Density Flow in Hypersonic Impulse Facilities*

1. [James Faddy MSc Thesis, 2000](/pdfs/james-faddy-masters-thesis-aug-2000.pdf) *Computational Modelling for Shock Tube Flows*

1. [Chris Craddock PhD Thesis, 1999](/pdfs/chris-craddock-phd-thesis-aug-1999.pdf) *Computational Optimization of Scramjets and Shock Tunnel Nozzles*

1. [Haruko Ishikawa PhD Thesis, 1999](/pdfs/haruko-ishikawa-phd-thesis-dec-1999.pdf) *Investigations of Optimum Design of Heat Exchangers of Thermoacoustic Engines*

1. [Ian-Johnston-PhD-Thesis, 1999](/pdfs/ian-johnston-phd-thesis-jan-1999-export.pdf) *Simulation of Flow Around Blunt-Nosed Vehicles for the Calibration of Air-Data Systems*

1. [Paul Petrie Repar PhD Thesis, 1997](/pdfs/paul-petrie-repar-phd-thesis-dec-1997.pdf) *Numerical Simulation of Diaphragm Rupture*

1. [Peter Jacobs PhD Thesis, 1987](/pdfs/peter-jacobs-phd-thesis-may-1987.pdf) *Nonlinear Dynamics of Piecewise-Constant Vorticity Distributions in an Inviscid Fluid*

1. [Neil Mudford PhD Thesis, 1976](/pdfs/neil-mudford-phd-thesis-production-of-pulsed-nozzle-flows.pdf) *The Production of Pulsed Nozzle Flows in a Shock Tube*

<!--- 2021-06-08, Commented out by RJG while I decide how to go about updating.

## Undergraduate Projects
Although the following documents are not directly linked, they should be available if you ask.

### Eilmer4

1. Alexander Braiden, 2016: *Computational Study of Earth Re-entry Flows.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Rowan Gollan]

1. Heather Muir, 2016: *An Unstructured Mesh Generation Code for Eilmer4.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Peter Jacobs]

1. Jonathan Ho, 2015: *Simulation of Dense Gas Flows in Computational Fluid Dynamics.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Peter Jacobs]


### Eilmer3

1. Charles McMahon, 2016: *Non-equilibrium Gas Effects in the Flow about Hypersonic Vehicles.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Rowan Gollan]

1. Matthew Trudigan, 2016: *Simualtion of moving flap in Eilmer 3.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Ingo Jahn]

1. Alex Ward, 2016: *Parametric Grid Generation for SPARTAN.*  Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Ingo Jahn]

1. Justin Beri, 2015: *Development of a Solid Solver for Tightly Coupled Conjugate Heat Transfer.* Undergradudate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Anand Veeraragavan]

1. James Burgess, 2015: *CFD Study of a Fast Opening Valve as a Film Diaphragm Replacement for Expansion Tubes.*  Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by David Gildfind]

1. Kyle Damm, 2015: *Using GPUs to Reduce Wall-Clock Times of Reacting Flow Simulations.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Anand Veeraragavan]

1. Lachlan Davies, 2015: *Simulation of Radiating Hypersonic Flows.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Rowan Gollan]

1. Douglas Irvine, 2015: *Numerical Investigation of a Cone Pressure Probe in Hypersonic Flows.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Rowan Gollan]

1. Andrew Williams, 2015: *Simulation of Pitching Aerofoil in Hypersonic Flow.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Ingo Jahn]

1. Lamboo, S., 2015: *Grid importing for CFD simulations of turbomachines.*  University of Twente Internship Report.

1. Samuel Stennet, 2014: *The Validation of the k-w Turbulence Model for 3D Test Cases.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Wilson Chan]

1. Campmans, G.H.P., 2013: *Validating dense-gas models recently added to the Computational Fluid Dynamics code Eilmer3 at University of Queensland.* University of Twente Thesis.

1. Zachary Denman, 2013: *Modelling Heat Recirculation in Micro-Combustors Using Eilmer3.* Undergraduate Thesis. School of Engineering. The University of Queensland. [Supervised by Peter Jacobs and Anand Veeraragavan]

1. Antonia Flocco, 2013: *Optimising Porthole Geometry for Maximum Jet Strength.* Undergraduate Thesis. School of Engineering. The University of Queensland. [Supervised by Ingo Jahn]

1. Nicholas Gibbons, 2013: *Computational Fluid Dynamics of Free-Piston Motion in Superorbital Expansion Tubes using the Ghost Fluid Method.* Undergraduate Thesis. School of Engineering. The University of Queensland. [Supervised by Vincent Wheatley]

1. Andrew Jensen, 2013: *Parallel Plate Microcombustion Simulations Using Eilmer3.* Undergraduate Thesis. School of Engineering, The University of Queensland. [Supervised by Anand Veeraragavan]

1. Peter Blyton, 2011: *Development of CFD Capability for the Optimisation of Radial-inflow Turbine Geometry* B.Sc Thesis, School of Engineering, The University of Queensland. [Supervised by Peter Jacobs]

1. Nathan Belgrove, 2010: *Development of CFD capability for the calculation of compressible flow in complex three-dimensional geometries.* B.A. Thesis, School of Engineering, The University of Queensland. [Supervised by Peter Jacobs]

1. Brian Cook, 2010: *Compressible-flow CFD exercise book for beginners in hypersonic flow analysis.* B.Sc Thesis, School of Engineering, The University of Queensland. [Supervised by Peter Jacobs]


### L1d3

1. Aaliya Doolabh, 2016:  *Slow opening valve for L1d3.* Undergraduate Thesis. School of Mechanical & Mining Engineering. The University of Queensland. [Supervised by Ingo Jahn]

--->
