---
title: "User Guides for Eilmer"
description: "Eilmer User Guide"
lead: ""
date: 2020-10-06T08:48:57+00:00
lastmod: 2020-10-06T08:48:57+00:00
draft: false
images: []
menu:
  docs:
    parent: "eilmer"
weight: 20
toc: false
---

There is a main Eilmer User Guide and some supporting user guides,
all available as PDF.
They are linked here.

<div class="row">
   <div class="col-sm-5">
   <a href="/pdfs/eilmer-user-guide.pdf">
    <img src="/images/eilmer-user-guide-thumbnail.png" style="width:100%">
   </a>
   </div>
   <div class="col-sm-10">
   The <a href="/pdfs/eilmer-user-guide.pdf"> Eilmer User Guide</a> is a good place for getting started with the simulation code.
   </div>
</div>

<br>

<div class="row">
   <div class="col-sm-5">
   <a href="/pdfs/geometry-user-guide.pdf">
    <img src="/images/geometry-user-guide-thumbnail.png" style="width:100%">
   </a>
   </div>
   <div class="col-sm-10">
   The <a href="/pdfs/geometry-user-guide.pdf"> Geometry User Guide</a> contains details
   on the geometric elements available in your Eilmer input scripts for building
   Eilmer-native grids.
   </div>
</div>

<br>

<div class="row">
   <div class="col-sm-5">
   <a href="/pdfs/gas-user-guide.pdf">
    <img src="/images/gas-user-guide-thumbnail.png" style="width:100%">
   </a>
   </div>
   <div class="col-sm-10">
   The <a href="/pdfs/gas-user-guide.pdf"> Gas User Guide</a> inroduces the
   basic gas modelling capabilities available for Eilmer.
   </div>
</div>

<br>

<div class="row">
   <div class="col-sm-5">
   <a href="/pdfs/reacting-gas-guide.pdf">
    <img src="/images/reacting-gas-guide-thumbnail.png" style="width:100%">
   </a>
   </div>
   <div class="col-sm-10">
   The <a href="/pdfs/reacting-gas-guide.pdf"> Reacting Gas Thermochemistry Guide</a> 
   is useful for those wanting to do reacting flow simulations.
   It contains information on the thermally-perfect gas model and how to
   construct a finite-rate chemistry input file.
   
   </div>
</div>
