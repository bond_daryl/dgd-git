---
title : "Gas Dynamics Toolkit"
description: "Computer software that makes gas dynamics calculations simple"
lead: "GDTk is a collection of software for doing gas dynamics, from simple desktop calculations through to simulations on supercomputers"
date: 2021-07-03
lastmod: 2020-07-03
draft: false
images: []
---
