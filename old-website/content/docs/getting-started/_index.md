---
title: "Getting started"
date: 2020-09-18
weight: 50
---

# Getting started with the toolkit

The programs are available as source code and
will need to be compiled and installed into a suitable location
before they can be used.
  + [Quick Start Guide]({{< relref "quick-start" >}}) offers an introduction to
    installing and using the programs on a Linux-like system.
    It gets you to the point of running a short simulation with the Eilmer flow code.
    Read this first.
  + [Installing LDC]({{< relref "installing-ldc" >}}) discusses installation of the
    LDC variant of the D-language compiler.
  + [Install and Run on HPC]({{< relref "install-and-run-on-hpc" >}}) describes
    the installation and use of the programs for a few of the HPC cluster computers
    that we use.
    This is a level up on using the programs on your personal workstation.
    Typically, you will have to submit your jobs to a batch queue.
  + [WSL2 Notes]({{< relref "wsl2-notes" >}}) gives a few pointers to getting
    Windows Subsystem for Linux set up on your Windows-10 workstation.


