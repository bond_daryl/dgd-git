---
weight: 20
---

# Reference manuals
These are online documents for reference as you build the scripts for your simulations.

- [Eilmer4 flow solver](/html/eilmer-reference-manual.html)
- [L1d4 flow solver](/html/l1d-reference-manual.html)
- [Gas-dynamic library for Lua and Python](/html/library-reference-manual.html)
- [Geometry library for Python](/html/geometry-reference-manual.html)
- [ESTCN, Equilibrium Shock Tunnel Condition, with Nozzle](/html/estcn-manual.html)
- [build-uniform-lut, for building look-up-table gas models](/html/build-lut-manual.html)
- [Python numerical-methods package](/html/nm-reference-manual.html)

