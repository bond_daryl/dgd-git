---
weight: 10
---

# User Guides
The first documentation that we would like you to read is the user-guide for each of the tools.
These guides are typically in PDF, suitable for printing onto paper,
so that you may read them away from the computer.


## Gas models and chemistry
A gas model is a core component of all of our flow-simulation tools.

- [Gas Package User Guide](/pdfs/gas-user-guide.pdf)
- [Reacting Gas Thermochemistry with the thermally-perfect-gas model](/pdfs/reacting-gas-guide.pdf)


## Geometry definition
The geometry package can be used for Eilmer and OpenFOAM simulations.

- [Geometry Package User Guide](/pdfs/geometry-user-guide.pdf)


## Eilmer flow solver
- [Eilmer4 Flow Solver User Guide](/pdfs/eilmer-user-guide.pdf)


## foamMesh
- [foamMesh User Guide](/pdfs/foammesh-user-guide.pdf)


