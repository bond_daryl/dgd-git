---
title: "Examples and FAQ"
weight: 40
---

# Examples and FAQ
You might want to browse the examples, to see if there is something close to your interest.

- [Eilmer4 flow solver](/html/eilmer-catalog-of-examples.html)
- [Eilmer4 Frequently-asked questions](/html/eilmer-faq.html)

