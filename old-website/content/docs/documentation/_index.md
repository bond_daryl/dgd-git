---
title: "Documentation"
weight: 50
---

# Documentation

The GTDK documentation comes in a variety of styles, each for different purposes:
  + [The User Guides]({{< relref "user-guides" >}}) offer an introduction to use
    of the various simulation programs and supporting modules in the toolkit.
    Read these first.
  + [The Reference Manuals]({{< relref "reference-manuals" >}}) are a terse
    (but eventually comprehensive) documentation of all of the input parameters and functions
    available to the users.
    These are for reference as you build your input scripts.
  + [Presentations and seminars]({{< relref "presentations" >}}) hosts a
    collection of presentations that the developers have made over the years
    about the development and use of the toolkit.
  + [Examples and FAQ]({{< relref "examples" >}}) provides a catalog
    of the examples and a list of frequently-asked questions.


