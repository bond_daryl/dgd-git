---
title: "Contributors"
weight: 100
draft: false
---

# Contributors
Eilmer is maintained by a group of co-chief gardeners:
+ Peter A. Jacobs,
+ Rowan J. Gollan,
+ Kyle Damm,
+ Wilson YK Chan,
+ Nick Gibbons,
+ Ingo Jahn,
+ Fabian Zander,
+ Anand Veeraragavan, and
+ Vince Wheatley

with contributions from a cast of many, including:

Ghassan Al'Doori,
Steven Apirana,
Nikhil Banerji,
Justin Beri,
Peter Blyton,
Daryl Bond,
Viv Bone,
Jamie Border,
Arianna Bosco,
Djamel Boutamine,
Laurie Brown,
James Burgess,
David Buttsworth,
Eric WK Chang,
Sam Chiu,
Chris Craddock,
Brian Cook,
Tim Cullen,
Damian Curran,
Jason Czapla,
Andrew Dann,
Andrew Denman,
Zac Denman,
Luke Doherty,
Elise Fahy,
Antonia Flocco,
Delphine Francois,
James Fuata,
Nick Gibbons,
David Gildfind,
Richard Gooze&eacute;,
Sangdi Gu,
Birte Haker,
Stefan Hess,
Jonathan Ho,
Carolyn Jacobs,
Juanita Jacobs,
Chris James,
Ian Johnston,
Ojas Joshi,
Xin Kang,
Rory Kelly,
Rainer Kirchhartz,
Sam Lamboo,
Will Landsberg,
Alexis Lefevre,
Steven Lewis,
Yu (Daisy) Liu,
Kieran Mackle,
Michael Macrossan,
Pierre Mariotto,
Tom Marty,
Matt McGilvray,
Sarah Mecklem,
David Mee,
Carlos de Miranda-Ventura,
Christine Mittler,
Luke Montgomery,
Richard G. Morgan,
Heather Muir,
Jan-Pieter Nap,
Brendan O'Flaherty,
Reece Otto,
Andrew Pastrello,
Oliver Paxton,
Paul Petrie-Repar,
Jorge Sancho Ponce,
Daniel F. Potter,
Jason (Kan) Qin,
Deepak Ramanath,
Andrew Rowlands,
Jacob Sandral,
Michael Scott,
Umar Sheikh,
Daniel Smith,
Tamara Sopek,
Sam Stennett,
Ben Stewart,
Oliver Street,
Joseph Tang,
Katsu Tanimizu,
Matthew Thompson,
Augustin Tib&egrave;re--Inglesse,
Pierpaolo Toniato,
Matt Trudgian,
Paul van der Laan,
Tjarke van Jindelt,
Jaidev Vesudevan,
Alex Ward,
Han Wei,
Mike Wendt,
Brad (The Beast) Wheatley,
Ryan Whitside,
Lachlan Whyborn,
Adriaan Window,
Hannes Wojciak,
Mengmeng Zhao

Over the years, these contributions have come in the form of examples, debugging,
proof-reading and constructive comments on the codes and the user guides,
additions to the user guides and code for special cases.


