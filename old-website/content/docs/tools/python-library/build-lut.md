---
title: LUT Builder
weight: 50
draft: false
---

## Build a Look-up-table gas model

`build-uniform-lut` assembles a table of gas-model data
computed by the CEA2 program.


## Documentation
- [build-uniform-lut Manual (HTML)](/html/build-lut-manual.html)


