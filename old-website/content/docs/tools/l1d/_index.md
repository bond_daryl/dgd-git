---
weight: 20
---

# L1d: Lagrangian Quasi-1D flow simulation

![](/images/L1d_tube_analysis_plot_m10p0_plot_600dpi_xshift.png)
Space-time diagram of wave processes in the X2 expansion tube for a Mach 10, high total-pressure operating condition. Simulation and figure preparation by David Gildfind, 2012.

## What does L1d do?

`l1d` is our specialist simulation code for quasi-one-dimensional gas dynamics.
It is usually applied to the simulation of entire shock-tube or expansion-tube facilities
and has a role to play in the design of new experimental machines.

## Documentation
- [L1d4 Reference Manual (HTML)](/html/l1d-reference-manual.html)
